#include <iostream>
#include <iomanip>
#include <vector>
#include <assert.h>
#include <cmath>
#include <sys/time.h>

#include <particle_simulator.hpp>

struct FPGrav{
	PS::S64    id;
	PS::F64    rad, mass;
	PS::F64vec pos, vel;
};


int main(void){
	//PS::ParticleSystem<FPGrav> ptcl;
	std::vector<FPGrav> ptcl;

	while(std::cin.eof() == false){
		FPGrav ith;
		std::cin >> ith.id;
		std::cin >> ith.mass;
		std::cin >> ith.rad;
		std::cin >> ith.pos.x;
		std::cin >> ith.pos.y;
		std::cin >> ith.pos.z;
		std::cin >> ith.vel.x;
		std::cin >> ith.vel.y;
		std::cin >> ith.vel.z;
		ptcl.push_back(ith);
	}
	
	const double dt = 1.0/512.;
	const double density = ptcl[0].mass / (4.0 * M_PI / 3.0 * pow(ptcl[0].rad, 3));
	for(int i = 0 ; i < ptcl.size() ; ++ i){
		ptcl[i].pos += ptcl[i].vel * dt;
	}
	for(int loop = 0 ; loop < 10 ; ++ loop){
		int cnt = 0;
		for(int i = 0 ; i < ptcl.size() ; ++ i){
			for(int j = 0 ; j < ptcl.size() ; ++ j){
				if(ptcl[i].id == ptcl[j].id) continue;
				if(1.0 * (ptcl[i].rad + ptcl[j].rad) >= sqrt((ptcl[i].pos - ptcl[j].pos) * (ptcl[i].pos - ptcl[j].pos))){
					FPGrav clump;
					clump.mass = ptcl[i].mass + ptcl[j].mass;
					clump.pos = (ptcl[i].mass * ptcl[i].pos + ptcl[j].mass * ptcl[j].pos) / clump.mass;
					clump.vel = (ptcl[i].mass * ptcl[i].vel + ptcl[j].mass * ptcl[j].vel) / clump.mass;
					clump.id = ptcl[i].id;
					clump.rad = cbrt(clump.mass / (4.0 * M_PI / 3.0 * density));
					//std::cout << ptcl[i].rad << "\t" << ptcl[j].rad << "\t" << clump.rad << std::endl;
					ptcl[j] = ptcl.back();
					ptcl.pop_back();
					j --;
					ptcl[i] = clump;
					++ cnt;
				}
			}
		}
		if(cnt == 0) break;
	}

	for(int i = 0 ; i < ptcl.size() ; ++ i){
		std::cout << ptcl[i].mass << "\t" << ptcl[i].rad << "\t" << ptcl[i].pos << std::endl;
	}

	return 0;
}

