#pragma once

class FileHeader{
public:
	PS::S64 Nbody;
	PS::F64 time;
	PS::S32 readAscii(FILE* fp) {
		fscanf(fp, "%lf\n", &time);
		fscanf(fp, "%lld\n", &Nbody);
		return Nbody;
	}
	void writeAscii(FILE* fp) const {
		fprintf(fp, "%e\n", time);
		fprintf(fp, "%lld\n", Nbody);
	}
};

struct system_t{
	double time;
	double dt;
	unsigned int step;
	double end_time;
	system_t() : time(0.0), step(0), end_time(0.0), dt(1.0e+30){
	}
};

//Self gravity
class ForceGrav{
	public:
	PS::F64vec acc;
	PS::F64vec jlt;
	PS::F64    pot;
	void clear(){
		acc = 0.0;
		jlt = 0.0;
		pot = 0.0;
	}
};

class ForceColl{
	public:
	PS::F64vec pos;
	PS::F64vec vel;
	PS::F64    ndens;
	void clear(){
		pos = vel = ndens = 0;
	}
};

class FPGrav{
public:
	PS::S64    id;
	PS::F64    mass;
	PS::F64vec pos, pos_next;
	PS::F64vec vel, vel_next;
	PS::F64vec acc, acc_next;
	PS::F64vec jlt, jlt_next;
	PS::F64vec snp;//snap
	PS::F64vec crc;//srackle
	PS::F64    pot;//potential
	PS::F64    rad;//radius
	PS::F64    eps;
	PS::F64    ndens;

	PS::F64vec getPos() const{
		return pos_next;
	}
	PS::F64 getCharge() const{
		return mass;
	}
	void copyFromForce(const ForceGrav& force){
		acc_next = force.acc;
		jlt_next = force.jlt;
		pot      = force.pot;
	}
	void copyFromForce(const ForceColl& diff){
		pos += diff.pos;
		vel += diff.vel;
		ndens = diff.ndens;
	}
	void predict(const PS::F64 dt_glb){
		pos_next = pos + vel * dt_glb + acc * dt_glb * dt_glb / 2.0 + jlt * dt_glb * dt_glb * dt_glb / 6.0;
		vel_next = vel + acc * dt_glb + jlt * dt_glb * dt_glb / 2.0;
	}
	void Hermite(const PS::F64 dt_glb){
		snp = (-6.0 * (acc - acc_next) - dt_glb * (4.0 * jlt + 2.0 * jlt_next)) / (dt_glb * dt_glb);
		crc = (12.0 * (acc - acc_next) + 6.0 * dt_glb * (jlt + jlt_next)) / (dt_glb * dt_glb * dt_glb);
	}
	void correct(const PS::F64 dt_glb){
		pos_next = pos + vel * dt_glb + acc * dt_glb * dt_glb / 2.0 + jlt * dt_glb * dt_glb * dt_glb / 6.0 + snp * dt_glb * dt_glb * dt_glb * dt_glb / 24.0 + crc * dt_glb * dt_glb * dt_glb * dt_glb * dt_glb / 120.0;
		vel_next = vel + acc * dt_glb + jlt * dt_glb * dt_glb / 2.0 + snp * dt_glb * dt_glb * dt_glb / 6.0 + crc * dt_glb * dt_glb * dt_glb * dt_glb / 24.0;
		pot      = pot + mass / eps;
	}
	void update(){
		pos = pos_next;
		vel = vel_next;
		acc = acc_next;
		jlt = jlt_next;
	}
	void writeAscii(FILE* fp) const{
		fprintf(fp, "%lld\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n", id, mass, rad, pos.x, pos.y, pos.z, vel.x, vel.y, vel.z);
	}
	void readAscii(FILE* fp){
		fscanf(fp, "%lld\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", &this->id, &this->mass, &this->rad, &this->pos.x, &this->pos.y, &this->pos.z, &this->vel.x, &this->vel.y, &this->vel.z);
		//fscanf(fp, "%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", &this->mass, &this->rad, &this->pos.x, &this->pos.y, &this->pos.z, &this->vel.x, &this->vel.y, &this->vel.z);
	}
};

class EPIGrav{
	public:
	PS::F64vec pos;
	PS::F64vec vel;
	PS::F64    eps;
	PS::F64vec getPos() const{
		return pos;
	}
	void copyFromFP(const FPGrav& rp){
		pos  = rp.pos_next;
		vel  = rp.vel_next;
		eps  = rp.eps;
	}
};

class EPJGrav{
	public:
	PS::F64vec pos;
	PS::F64vec vel;
	PS::F64    mass;
	PS::F64vec getPos() const{
		return pos;
	}
	PS::F64 getCharge(void) const{
		return mass;
	}
	void copyFromFP(const FPGrav& rp){
		pos  = rp.pos_next;
		vel  = rp.vel_next;
		mass = rp.mass;
	}
};

class EPIColl{
	public:
	PS::S64    id;
	PS::F64    mass;
	PS::F64vec pos;
	PS::F64vec vel;
	PS::F64    rad;
	PS::F64vec getPos() const {
		return pos;
	}
	PS::F64 getCharge() const {
		return mass;
	}
	void copyFromFP(const FPGrav & fp){
		id   = fp.id;
		mass = fp.mass;
		pos  = fp.pos;
		vel  = fp.vel;
		rad  = fp.rad;
	}
	PS::F64 getRSearch() const{
		return 2.0 * rad;
	}
};

class EPJColl{
	public:
	PS::S64    id;
	PS::F64    mass;
	PS::F64vec pos;
	PS::F64vec vel;
	PS::F64    rad;
	PS::F64vec getPos() const {
		return pos;
	}
	PS::F64 getCharge() const {
		return mass;
	}
	void setPos(const PS::F64vec& pos){
		this->pos = pos;
	}
	void copyFromFP(const FPGrav & fp){
		id   = fp.id;
		mass = fp.mass;
		pos  = fp.pos;
		vel  = fp.vel;
		rad  = fp.rad;
	}
	PS::F64 getRSearch() const{
		return 2.0 * rad;
	}
};


class MomentMonopoleWithVelocity{
	public:
	PS::F64 mass;
	PS::F64vec pos;
	PS::F64vec vel;
	MomentMonopoleWithVelocity(){
		mass = 0.0;
		pos = 0.0;
		vel = 0.0;
	}
	MomentMonopoleWithVelocity(const PS::F64& m, const PS::F64vec& p, const PS::F64vec& v){
		mass = m;
		pos  = p;
		vel  = v;
	}
	void init(){
		mass = 0.0;
		pos = 0.0;
		vel = 0.0;
	}
	PS::F64vec getPos() const{
		return pos;
	}
	PS::F64 getCharge() const{
		return mass;
	}
	template<class Tepj> void accumulateAtLeaf(const Tepj& epj){
		mass += epj.mass;
		pos  += epj.mass * epj.pos;
		vel  += epj.mass * epj.vel;
	}
	template<class Tepj> void accumulateAtLeaf2(const Tepj& epj){
	}
	void set(){
		pos = pos / mass;
		vel = vel / mass;
	}
	void accumulate(const MomentMonopoleWithVelocity& mom){
		mass += mom.mass;
		pos += mom.mass * mom.pos;
		vel += mom.mass * mom.vel;
	}
	void accumulate2(const MomentMonopoleWithVelocity& mom){
	}
};

class SPJMonopoleWithVelocity{
	public:
	PS::F64 mass;
	PS::F64vec pos;
	PS::F64vec vel;
	template<class Tmom> void copyFromMoment(const Tmom& mom){
		mass = mom.mass;
		pos  = mom.pos;
		vel  = mom.vel;
	}
	void clear(){
		mass = 0.0;
		pos = 0.0;
		vel = 0.0;
	}
	PS::F64 getCharge() const{
		return mass;
	}
	PS::F64vec getPos() const{
		return pos;
	}
	void setPos(const PS::F64vec& pos_new){
		pos = pos_new;
	}
	MomentMonopoleWithVelocity convertToMoment() const{
		return MomentMonopoleWithVelocity(mass, pos, vel);
	}
};

