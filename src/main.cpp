#include <iostream>
#include <iomanip>
#include <vector>
#include <assert.h>
#include <cmath>
#include <sys/time.h>

#include <particle_simulator.hpp>
#include "class_platform.hpp"

#ifdef ENABLE_PEZY
#define MULTI_WALK
#include <pzcl/pzcl_ocl_wrapper.h>
#include <PZSDKHelper.h>
#include "class_device.hpp"

const int N_WALK_LIMIT = 128;
const int NI_LIMIT = 100000;
const int NJ_LIMIT = 100000;
const int N_THREAD_MAX = 8192;

struct{
	int j_disp[N_WALK_LIMIT+2];
	EpiDev epi[NI_LIMIT];
	EpjDev epj[NJ_LIMIT];
	ForceDev force[NI_LIMIT];
}host;

static bool LoadFile(const char* name, size_t size, char* pData){
	FILE* fp = fopen(name, "rb");
	if(fp == NULL){
		printf("can not open %s\n", name);
		return false;
	}

	if(size == 0 || pData == NULL){
		printf("invalid params %s\n", __FUNCTION__);
		return false;
	}

	size_t size_ret = fread(pData, sizeof(char), size, fp);
	fclose(fp);

	if(size_ret != size){
		printf("can not read requested size\n");
		return false;
	}
	return true;
}

static size_t GetFileSize(const char* name){
	FILE* fp = fopen(name, "rb");
	if(fp == NULL){
		printf("can not open %s", name);
		return 0;
	}
	fseek(fp, 0, SEEK_END);
	size_t size = ftell(fp);
	fclose(fp);
	return size;
}

cl_program CreateProgram(cl_context context, std::vector<cl_device_id> &device_id_lists, const char* bin_name){
	cl_program program = NULL;
	char* pBin = NULL;
	cl_int result;

	size_t sizeFile = GetFileSize(bin_name);
	if(sizeFile == 0){
		goto leaving;
	}

	PZSDK_ALIGNED_ALLOC(pBin, sizeFile, 8 /*8 byte alignment*/);
	if(pBin == NULL){
		printf("out of host memory\n");
		goto leaving;
	}

	if(!LoadFile(bin_name, sizeFile, pBin)){
		goto leaving;
	}

	{
		const unsigned char* listBin[1];
		listBin[0] = (unsigned char*)pBin;
		cl_int binary_status = CL_SUCCESS;
		size_t length = sizeFile;

		program = clCreateProgramWithBinary(context, (cl_uint)device_id_lists.size(), &device_id_lists[0], &length, listBin, &binary_status, &result);
	}
	if(program == NULL){
		printf("clCreateProgramWithBinary failed, %d\n", result);
		goto leaving;
	}
leaving:
	if(pBin){
		PZSDK_ALIGNED_FREE(pBin);
	}
	return program;
}

struct PezyDevice{
	static const int NumDeviceMax = 4;
	//on device vars.
	cl_mem j_disp;
	cl_mem epi;
	cl_mem epj;
	cl_mem force;
	//device info.
	cl_platform_id   platform_id;
	cl_uint          num_of_platforms, num_of_devices;
	cl_device_id     device_id[NumDeviceMax];
	cl_context       context;
	cl_command_queue cmd_queue;
	cl_program       program;
	cl_kernel        kernel;

	std::vector<cl_device_id> device_id_list;

	void initialize(){
		cl_int return_code;
		int rank = PS::Comm::getRank();
		return_code = clGetPlatformIDs(1, &platform_id, &num_of_platforms);
		std::cout << "clGetPlatformIDs at #" << rank << ": " << return_code << std::endl;
		return_code = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, NumDeviceMax, device_id, &num_of_devices);
		std::cout << "clGetDeviceIDs at #" << rank << ": " << return_code << std::endl;
		context     = clCreateContext(NULL, 1, &device_id[rank % NumDeviceMax], NULL, NULL, &return_code);
		std::cout << "clCreateContext at #" << rank << ": " << return_code << std::endl;
		cmd_queue   = clCreateCommandQueue(context, device_id[rank % NumDeviceMax], 0, &return_code);
		std::cout << "clCreateCommandQueue at #" << rank << ": " << return_code << std::endl;

		std::cout << "Platform(" << platform_id << ") # " << num_of_platforms << std::endl;
		std::cout << "Device  (" << device_id[rank]   << ") # " << num_of_devices << std::endl;

		j_disp = clCreateBuffer(context, CL_MEM_READ_WRITE, (N_WALK_LIMIT + 2) * sizeof(int), NULL, &return_code);
		std::cout << "clCreateBuffer at #" << rank << ": " << return_code << std::endl;
		epi    = clCreateBuffer(context, CL_MEM_READ_WRITE, NI_LIMIT * sizeof(EpiDev),        NULL, &return_code);
		std::cout << "clCreateBuffer at #" << rank << ": " << return_code << std::endl;
		epj    = clCreateBuffer(context, CL_MEM_READ_WRITE, NJ_LIMIT * sizeof(EpjDev),        NULL, &return_code);
		std::cout << "clCreateBuffer at #" << rank << ": " << return_code << std::endl;
		force  = clCreateBuffer(context, CL_MEM_READ_WRITE, NI_LIMIT * sizeof(ForceDev),      NULL, &return_code);
		std::cout << "clCreateBuffer at #" << rank << ": " << return_code << std::endl;

		device_id_list.push_back(device_id[rank % NumDeviceMax]);
		program = CreateProgram(context, device_id_list, "./kernel.sc32/kernel.pz");
		if(program == NULL){
			std::cerr << "can't create program" << std::endl;
			exit(1);
		}

		kernel = clCreateKernel(program, "GravityKernel", &return_code);
		if(kernel == NULL){
			std::cerr<<"can't create kernel"<<std::endl;
			exit(1);
		}
		return_code = clSetKernelArg(kernel, 4, sizeof(int), (void*)&N_THREAD_MAX);
	}
	~PezyDevice(){
		clReleaseMemObject(j_disp);
		clReleaseMemObject(epi);
		clReleaseMemObject(epj);
		clReleaseMemObject(force);

		clFlush(cmd_queue);
		clFinish(cmd_queue);
		clReleaseKernel(kernel);
		clReleaseProgram(program);
		clReleaseCommandQueue(cmd_queue);
		clReleaseContext(context);
	}
}device;

PS::S32 DispatchKernelWithSP(const PS::S32 tag, const PS::S32 n_walk, const EPIGrav* epi[], const PS::S32 Nepi[], const EPJGrav* epj[], const PS::S32 Nepj[], const SPJMonopoleWithVelocity* spj[], const PS::S32 Nspj[]){
	assert(n_walk <= N_WALK_LIMIT);
	cl_int return_code;
	PS::S32 ni_tot = 0;
	host.j_disp[0] = 0;
	for(int k = 0 ; k < n_walk ; ++ k){
		ni_tot += Nepi[k];
		host.j_disp[k + 1] = host.j_disp[k] + (Nepj[k] + Nspj[k]);
	}
	host.j_disp[n_walk + 1] = host.j_disp[n_walk];
	assert(ni_tot < NI_LIMIT);
	assert(host.j_disp[n_walk] < NJ_LIMIT);

	return_code = clEnqueueWriteBuffer(device.cmd_queue, device.j_disp, CL_TRUE, 0, (n_walk + 2) * sizeof(int), host.j_disp, 0, NULL, NULL);
	ni_tot = 0;
	int nj_tot = 0;
	for(int iw = 0 ; iw < n_walk ; ++ iw){
		for(int i = 0 ; i < Nepi[iw] ; ++ i){
			host.epi[ni_tot].rx  = epi[iw][i].pos.x;
			host.epi[ni_tot].ry  = epi[iw][i].pos.y;
			host.epi[ni_tot].rz  = epi[iw][i].pos.z;
			host.epi[ni_tot].vx  = epi[iw][i].vel.x;
			host.epi[ni_tot].vy  = epi[iw][i].vel.y;
			host.epi[ni_tot].vz  = epi[iw][i].vel.z;
			host.epi[ni_tot].eps = epi[iw][i].eps;
			host.epi[ni_tot].id_walk = iw;
			++ ni_tot;
		}
		for(int j = 0 ; j < Nepj[iw] ; ++ j){
			host.epj[nj_tot].rx   = epj[iw][j].pos.x;
			host.epj[nj_tot].ry   = epj[iw][j].pos.y;
			host.epj[nj_tot].rz   = epj[iw][j].pos.z;
			host.epj[nj_tot].vx   = epj[iw][j].vel.x;
			host.epj[nj_tot].vy   = epj[iw][j].vel.y;
			host.epj[nj_tot].vz   = epj[iw][j].vel.z;
			host.epj[nj_tot].mass = epj[iw][j].mass;
			++ nj_tot;
		}
		for(int j = 0 ; j < Nspj[iw] ; ++ j){
			host.epj[nj_tot].rx   = spj[iw][j].pos.x;
			host.epj[nj_tot].ry   = spj[iw][j].pos.y;
			host.epj[nj_tot].rz   = spj[iw][j].pos.z;
			host.epj[nj_tot].vx   = spj[iw][j].vel.x;
			host.epj[nj_tot].vy   = spj[iw][j].vel.y;
			host.epj[nj_tot].vz   = spj[iw][j].vel.z;
			host.epj[nj_tot].mass = spj[iw][j].mass;
			++ nj_tot;
		}
	}
	return_code = clEnqueueWriteBuffer(device.cmd_queue, device.epi, CL_TRUE, 0, (ni_tot) * sizeof(EpiDev), host.epi, 0, NULL, NULL);
	return_code = clEnqueueWriteBuffer(device.cmd_queue, device.epj, CL_TRUE, 0, (nj_tot) * sizeof(EpjDev), host.epj, 0, NULL, NULL);

	return_code = clSetKernelArg(device.kernel, 0, sizeof(cl_mem), (void*)&device.j_disp);
	return_code = clSetKernelArg(device.kernel, 1, sizeof(cl_mem), (void*)&device.epi);
	return_code = clSetKernelArg(device.kernel, 2, sizeof(cl_mem), (void*)&device.epj);
	return_code = clSetKernelArg(device.kernel, 3, sizeof(cl_mem), (void*)&device.force);
	return_code = clSetKernelArg(device.kernel, 4, sizeof(int)   , (void*)&ni_tot);

	size_t work_size = N_THREAD_MAX;
	return_code = clEnqueueNDRangeKernel(device.cmd_queue, device.kernel, 1, NULL, &work_size, NULL, 0, NULL, NULL);
	return 0;
}

PS::S32 RetrieveKernel(const PS::S32 tag, const PS::S32 n_walk, const PS::S32 ni[], ForceGrav* force[]){
	cl_int return_code;
	int ni_tot = 0;
	for(int k = 0 ; k < n_walk ; ++ k){
		ni_tot += ni[k];
	}
	return_code = clEnqueueReadBuffer(device.cmd_queue, device.force, CL_TRUE, 0, ni_tot * sizeof(ForceDev), host.force, 0, NULL, NULL);
	int cnt = 0;
	for(int w = 0 ; w < n_walk ; ++ w){
		for(int i = 0;  i < ni[w] ; ++ i){
			force[w][i].acc.x = host.force[cnt].ax;
			force[w][i].acc.y = host.force[cnt].ay;
			force[w][i].acc.z = host.force[cnt].az;
			force[w][i].jlt.x = host.force[cnt].jx;
			force[w][i].jlt.y = host.force[cnt].jy;
			force[w][i].jlt.z = host.force[cnt].jz;
			force[w][i].pot   = host.force[cnt].pot;
			++ cnt;
		}
	}
	return 0;
}
#endif

void SetIC(PS::ParticleSystem<FPGrav>& ptcl, system_t& sysinfo){
	sysinfo.end_time = 1000.0;
	FileHeader header;
	ptcl.readParticleAscii("moon_ic.txt", header);
	/*
	double time;
	int N;
	std::cin >> time;
	std::cin >> N;
	ptcl.setNumberOfParticleLocal(N);
	int cnt = 0;
	while(!std::cin.eof()){
		FPGrav ith;
		std::cin >> ith.id;
		std::cin >> ith.mass;
		std::cin >> ith.rad;
		std::cin >> ith.pos.x;
		std::cin >> ith.pos.y;
		std::cin >> ith.pos.z;
		std::cin >> ith.vel.x;
		std::cin >> ith.vel.y;
		std::cin >> ith.vel.z;
		ptcl[cnt++] = ith;
		std::cout << ith.id << " " << ith.rad << " "  << ith.pos << std::endl;
	}
	*/
}

template <class ThisPtcl> void CheckConservativeVars(const PS::ParticleSystem<ThisPtcl>& ptcl, const PS::F64 time){
	PS::F64vec com, mom;
	PS::F64 Ekin, Epot, Mass;
	com = mom = 0;
	Ekin = Epot = Mass = 0;
	PS::F64 Cnt = 0;
	for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		Mass += ptcl[i].mass;
		com  += ptcl[i].mass * ptcl[i].pos;
		mom  += ptcl[i].mass * ptcl[i].vel;
		Ekin += 0.5 * ptcl[i].mass * ptcl[i].vel * ptcl[i].vel;
		Epot += 0.5 * ptcl[i].mass * ptcl[i].pot;
		Cnt += ptcl[i].ndens;
	}
	Mass = PS::Comm::getSum(Mass);
	com = PS::Comm::getSum(com);
	mom = PS::Comm::getSum(mom);
	Ekin = PS::Comm::getSum(Ekin);
	Epot = PS::Comm::getSum(Epot);
	if(PS::Comm::getRank() == 0){
		std::cout << "Mass: " << Mass << std::endl;
		std::cout << "com : " << com / Mass << std::endl;
		std::cout << "mom : " << mom  << std::endl;
		std::cout << "Ekin: " << Ekin << std::endl;
		std::cout << "Epot: " << Epot << std::endl;
		std::cout << "Etot: " << Ekin + Epot << std::endl;
		#if 1
		std::ofstream file;
		file.open("check.txt", std::ios::app);
		file << time << "\t" << com << "\t" << mom << "\t" << Cnt << std::endl;
		file.close();
		#endif
	}
}

template <class ThisPtcl> void OutputBinary(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
	//Binary
	char filename[256];
	std::ofstream fout;
	sprintf(filename, "result/%05d_%05d_%05d.bin", sysinfo.step % 10, PS::Comm::getNumberOfProc(), PS::Comm::getRank());
	fout.open(filename, std::ios::out | std::ios::binary | std::ios::trunc);
	if(!fout){
		std::cout << "cannot open restart file." << std::endl;
		exit(1);
	}
	fout.write(reinterpret_cast<const char * const>(&sysinfo), sizeof(system_t));
	for(std::size_t i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		ThisPtcl ith = ptcl[i];
		fout.write((char*)&ith, sizeof(ThisPtcl));
	}
	fout.close();
}

template <class ThisPtcl> void InputBinary(PS::ParticleSystem<ThisPtcl>& sph_system, system_t* sysinfo){
	char filename[256];
	const int step = sysinfo->step;
	sprintf(filename, "result/%05d_%05d_%05d.bin", sysinfo->step, PS::Comm::getNumberOfProc(), PS::Comm::getRank());
	std::ifstream fin(filename, std::ios::in | std::ios::binary);
	if(!fin){
		std::cout << "cannot open restart file." << std::endl;
		exit(1);
	}
	std::vector<ThisPtcl> ptcl;
	fin.read((char*)sysinfo, sizeof(system_t));
	sysinfo->step = step;
	while(1){
		ThisPtcl ith;
		fin.read((char*)&ith, sizeof(ThisPtcl));
		if(fin.eof() == true) break;
		ptcl.push_back(ith);
	}
	fin.close();
	sph_system.setNumberOfParticleLocal(ptcl.size());
	for(std::size_t i = 0 ; i < ptcl.size() ; ++ i){
		sph_system[i] = ptcl[i];
	}
}

template <class ThisPtcl> void OutputFileWithTimeInterval(PS::ParticleSystem<ThisPtcl>& ptcl, const system_t& sysinfo){
	static PS::F64 time = sysinfo.time;
	static PS::S64 step = sysinfo.step;
	const int NumberOfSnapshot = 1000;
	if(sysinfo.time >= time){
		FileHeader header;
		header.time = sysinfo.time;
		char filename[256];
		//header.Nbody = ptcl.getNumberOfParticleLocal();
		//sprintf(filename, "result/%05d", step);
		//ptcl.writeParticleAscii(filename, "%s_%05d_%05d.dat", header);
		header.Nbody = ptcl.getNumberOfParticleGlobal();
		sprintf(filename, "result/%05d.dat", step);
		ptcl.writeParticleAscii(filename, header);
		if(PS::Comm::getRank() == 0){
			std::cout << "//================================" << std::endl;
			std::cout << "output " << filename << "." << std::endl;
			std::cout << "//================================" << std::endl;
		}
		time += sysinfo.end_time / NumberOfSnapshot;
		++ step;
		CheckConservativeVars(ptcl, sysinfo.time);
		OutputBinary(ptcl, sysinfo);
	}
}

template <class ThisPtcl> void ShiftOrigin(PS::ParticleSystem<ThisPtcl>& ptcl){
	PS::F64 Mass;
	PS::F64vec com, mom;
	com = mom = 0;
	Mass = 0;
	for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		Mass += ptcl[i].mass;
		com  += ptcl[i].mass * ptcl[i].pos;
		mom  += ptcl[i].mass * ptcl[i].vel;
	}
	Mass = PS::Comm::getSum(Mass);
	com = PS::Comm::getSum(com);
	mom = PS::Comm::getSum(mom);
	com /= Mass;
	mom /= Mass;
	for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		ptcl[i].pos -= com;
		ptcl[i].vel -= mom;
	}
}

template <class ThisPtcl> void RemoveParticle(PS::ParticleSystem<ThisPtcl>& ptcl){
	int hasPlanet = 0;
	ThisPtcl* ptrPlanet = &(ptcl[0]);
	for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		if(ptcl[i].id == 0){
			hasPlanet = PS::Comm::getRank();
			ptrPlanet = &(ptcl[i]);
			continue;
		}
	}
	const int root = PS::Comm::getSum(hasPlanet);
	//clone of planet (should be const...)
	ThisPtcl buf = *ptrPlanet;
	PS::Comm::broadcast(&buf, 1, root);

	PS::F64 dmass = 0;
	PS::F64vec dpos = 0;
	PS::F64vec dvel = 0;

	for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		if((buf.pos - ptcl[i].pos) * (buf.pos - ptcl[i].pos) < (buf.rad + ptcl[i].rad) * (buf.rad + ptcl[i].rad) && buf.id != ptcl[i].id){
			dpos += (buf.mass * buf.pos + ptcl[i].mass * ptcl[i].pos) / (buf.mass + ptcl[i].mass) - buf.pos;
			dvel += (buf.mass * buf.vel + ptcl[i].mass * ptcl[i].vel) / (buf.mass + ptcl[i].mass) - buf.vel;
			dmass += ptcl[i].mass;
			ptcl[i] = ptcl[ptcl.getNumberOfParticleLocal() - 1];
			ptcl.setNumberOfParticleLocal(ptcl.getNumberOfParticleLocal() - 1);
			-- i;
		}
	}
	PS::Comm::barrier();
	dmass = PS::Comm::getSum(dmass);
	dpos  = PS::Comm::getSum(dpos);
	dvel  = PS::Comm::getSum(dvel);
	if(PS::Comm::getRank() == root){
		ptrPlanet->pos += dpos;
		ptrPlanet->vel += dvel;
		ptrPlanet->mass += dmass;
		//ptrPlanet->rad = ... ;
	}
	for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		ptcl[i].pos -= buf.pos;
	}
}

template <class TPtclJ> class CalcGravityForce{
	public:
	void operator () (const EPIGrav* const __restrict ep_i, const PS::S32 Nip, const TPtclJ* const __restrict ep_j, const PS::S32 Njp, ForceGrav* const grav){
		for(PS::S32 i = 0; i < Nip ; ++ i){
			const EPIGrav& ith = ep_i[i];
			for(PS::S32 j = 0; j < Njp ; ++ j){
				const TPtclJ& jth = ep_j[j];
				const PS::F64vec dr = ith.pos - jth.pos;
				const PS::F64vec dv = ith.vel - jth.vel;
				const PS::F64 dr2 = dr * dr /*+ ith.eps * jth.eps*/;
				if(dr2 <= 0.0) continue;
				const PS::F64 dr_inv = 1.0 / sqrt(dr2);
				const PS::F64 m_dr3_inv = jth.mass * pow(dr_inv, 3);
				const PS::F64 m_dr5_inv = m_dr3_inv / dr2;
				grav[i].acc -= m_dr3_inv * dr;
				grav[i].jlt -= m_dr3_inv * dv - 3.0 * (dr * dv) * m_dr5_inv * dr;
				grav[i].pot -= jth.mass * dr_inv;
			}
		}
	}
};

class CollisionDetect{
	const PS::F64 eps_n = 0.1;
	public:
	void operator () (const EPIColl* const ep_i, const PS::S32 Nip, const EPJColl* const ep_j, const PS::S32 Njp, ForceColl* const diff){
		for(PS::S32 i = 0 ; i < Nip ; ++ i){
			const EPIColl& ith = ep_i[i];
			for(PS::S32 j = 0 ; j < Njp ; ++ j){
				const EPJColl& jth = ep_j[j];
				const PS::F64vec dr = jth.pos - ith.pos;
				const PS::F64vec dv = jth.vel - ith.vel;
				const PS::F64 pd = ith.rad + jth.rad - sqrt(dr * dr);
				if(pd < 0 || dr * dr <= 0.0) continue;
				const PS::F64vec n  = dr / sqrt(dr * dr);
				diff[i].pos += - jth.mass / (ith.mass + jth.mass) * pd * n;
				diff[i].vel += (1.0 + eps_n) * jth.mass / (ith.mass + jth.mass) * (dv * n) * n;
				diff[i].ndens += 1.0;
			}
		}
	}
};

template <class ThisPtcl> double GetGlobalTimestep(const PS::ParticleSystem<ThisPtcl>& ptcl){
	PS::F64 dt = 1.0e+30;
	const PS::F64 eta = 1.0e-2;
	for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		const double vel = sqrt(ptcl[i].vel * ptcl[i].vel);
		const double acc = sqrt(ptcl[i].acc * ptcl[i].acc);
		const double jlt = sqrt(ptcl[i].jlt * ptcl[i].jlt);
		const double snp = sqrt(ptcl[i].snp * ptcl[i].snp);
		const double crc = sqrt(ptcl[i].crc * ptcl[i].crc);
		const double dti = (jlt == 0.0) ? 1.0e-16 : eta * sqrt((acc * snp + jlt * jlt + 1.0e-16)/(jlt * crc + snp * snp + 1.0e-8));
		dt = std::min(dti, dt);
	}
	dt = std::max(dt, 1.0e-16);
	return /* PS::Comm::getMinValue(dt) */ 1./512.;
}

double get_dtime(void){
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return ((double)(tv.tv_sec) + (double)(tv.tv_usec) * 0.001 * 0.001);
}

int main(int argc, char *argv[]){
	std::cout << std::scientific << std::setprecision(16);
	std::cerr << std::scientific << std::setprecision(16);
	PS::Initialize(argc, argv);
	#ifdef ENABLE_PEZY
	device.initialize();
	#endif
	PS::ParticleSystem<FPGrav> ptcl;
	ptcl.initialize();
	system_t sysinfo;
	PS::TreeForForce<PS::SEARCH_MODE_LONG, ForceGrav, EPIGrav, EPJGrav, MomentMonopoleWithVelocity, MomentMonopoleWithVelocity, SPJMonopoleWithVelocity> tree_grav;
	PS::TreeForForceShort<ForceColl, EPIColl, EPJColl>::Symmetry tree_coll;
	PS::DomainInfo dinfo;

	if(argc >= 2){
		sysinfo.step = atoi(argv[1]);
		std::cout << "restart ID: " << sysinfo.step << std::endl;
		InputBinary<FPGrav>(ptcl, &sysinfo);
		dinfo.initialize(0.3);
		ptcl.setAverageTargetNumberOfSampleParticlePerProcess(200);
		dinfo.collectSampleParticle(ptcl);
		dinfo.decomposeDomainAll(ptcl);
		ptcl.exchangeParticle(dinfo);
		tree_grav.initialize(ptcl.getNumberOfParticleLocal(), 0.5, 8, 128);
		tree_coll.initialize(ptcl.getNumberOfParticleLocal(), 0.5, 8, 128);
		goto RESTART;
	}else{
		SetIC(ptcl, sysinfo);
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			ptcl[i].pos_next = ptcl[i].pos;
			ptcl[i].vel_next = ptcl[i].vel;
			ptcl[i].eps = 1.0e-4;
		}
		ShiftOrigin(ptcl);
		dinfo.initialize(0.3);
		ptcl.setAverageTargetNumberOfSampleParticlePerProcess(200);
		dinfo.collectSampleParticle(ptcl);
		dinfo.decomposeDomainAll(ptcl);
		ptcl.exchangeParticle(dinfo);
		tree_grav.initialize(ptcl.getNumberOfParticleLocal(), 0.5, 8, 128);
		tree_coll.initialize(ptcl.getNumberOfParticleLocal(), 0.5, 8, 128);
	}


	#ifdef ENABLE_PEZY
		//std::cout << "PZ" << std::endl;
		tree_grav.calcForceAllAndWriteBackMultiWalk(DispatchKernelWithSP, RetrieveKernel, 1, ptcl, dinfo, N_WALK_LIMIT);
	#else
		//std::cout << "not PZ" << std::endl;
		tree_grav.calcForceAllAndWriteBack(CalcGravityForce<EPJGrav>(), CalcGravityForce<SPJMonopoleWithVelocity>(), ptcl, dinfo);
	#endif
	RemoveParticle(ptcl);
	tree_coll.calcForceAllAndWriteBack(CollisionDetect(), ptcl, dinfo);

	OutputFileWithTimeInterval<FPGrav>(ptcl, sysinfo);

	for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		ptcl[i].acc = ptcl[i].acc_next;
		ptcl[i].jlt = ptcl[i].jlt_next;
	}

	for(sysinfo.time = 0, sysinfo.step = 0 ; sysinfo.time < sysinfo.end_time ; sysinfo.time += sysinfo.dt, ++ sysinfo.step){
		sysinfo.dt = GetGlobalTimestep<FPGrav>(ptcl);
		#pragma omp parallel for
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			ptcl[i].predict(sysinfo.dt);
		}
		for(int loop = 0 ; loop < 3 ; ++ loop){
			double start_time = get_dtime();
			#ifdef ENABLE_PEZY
				//std::cout << "PZ" << std::endl;
				tree_grav.calcForceAllAndWriteBackMultiWalk(DispatchKernelWithSP, RetrieveKernel, 1, ptcl, dinfo, N_WALK_LIMIT);
			#else
				//std::cout << "not PZ" << std::endl;
				tree_grav.calcForceAllAndWriteBack(CalcGravityForce<EPJGrav>(), CalcGravityForce<SPJMonopoleWithVelocity>(), ptcl, dinfo);
			#endif
			std::cout << PS::Comm::getRank() << ":Grav " << (double)(get_dtime() - start_time) << std::endl;
			#pragma omp parallel for
			for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
				ptcl[i].Hermite(sysinfo.dt);
				ptcl[i].correct(sysinfo.dt);
			}
		}
		#pragma omp parallel for
		for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
			ptcl[i].update();
		}
		//double start_time = get_dtime();
		RemoveParticle(ptcl);
		tree_coll.calcForceAllAndWriteBack(CollisionDetect(), ptcl, dinfo);
		//std::cout << PS::Comm::getRank() << ":Coll " << (double)(get_dtime() - start_time) << std::endl;

		dinfo.decomposeDomainAll(ptcl);
		ptcl.exchangeParticle(dinfo);

		if(PS::Comm::getRank() == 0){
			std::cout << "#=======================" << std::endl;
			std::cout << sysinfo.time << "(dt = " << sysinfo.dt << ")" << std::endl;
			std::cout << "#=======================" << std::endl;
		}
		OutputFileWithTimeInterval<FPGrav>(ptcl, sysinfo);

		RESTART: ;

	}
	
	PS::Finalize();
	return 0;
}

