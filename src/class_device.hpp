typedef float real;

struct EpiDev{
	real rx;
	real ry;
	real rz;
	real vx;
	real vy;
	real vz;
	real eps;
	int id_walk;
};

struct EpjDev{
	real rx;
	real ry;
	real rz;
	real vx;
	real vy;
	real vz;
	real mass;
};
struct ForceDev{
	real ax;
	real ay;
	real az;
	real jx;
	real jy;
	real jz;
	real pot;
};

